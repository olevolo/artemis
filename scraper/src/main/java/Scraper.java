import model.News;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scraper {

    private final static String SOURCE = "http://pravda.com.ua";
    private final static String XPATH = "/html/body/div[4]/div[2]/div[1]/div[3]"; //"/html/body/div[1]/div[3]/div[4]/div/div[1]/div/div[1]/div[3]/div[2]/div/div[1]/div[1]";
    private Date lastNewsDate;
    static DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public Scraper() throws FileNotFoundException, ParseException {
        this.lastNewsDate = getLastDate();
    }

    public static Date getLastDate() throws FileNotFoundException, ParseException {
        Scanner scanner = new Scanner(new File("scraper/lastNewsDate.txt"));
        Date fileDate = df.parse(scanner.nextLine());
        scanner.close();
        return fileDate;
    }

    public static void setLastDate(String time) throws IOException {
        Date date = getDateFromTime(time);
        String output = df.format(date);
        writeLastDate(output);
    }

    public static Date getDateFromTime(String time) {
        Date date = new Date();
        String hours = time.split(":")[0];
        String minutes = time.split(":")[1];
        date.setHours(Integer.parseInt(hours.startsWith("0") ? hours.substring(1) : hours));
        date.setMinutes(Integer.parseInt(minutes.startsWith("0") ? minutes.substring(1) : minutes) + 1);
        return date;
    }

    public static void writeLastDate(String output) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("scraper/lastNewsDate.txt"));
        writer.write(output);
        writer.close();
    }
    private ChromeOptions chromeOptions = new ChromeOptions().addArguments("--headless");;
    private WebDriver webDriver = new ChromeDriver(chromeOptions);

    public List<String> scrap() {
        System.out.printf("Scrapping %s...\n", SOURCE);
        webDriver.get(SOURCE);
        List<WebElement> newsWebElement = webDriver.findElements(By.xpath(XPATH));
        List<String> news = newsWebElement.stream()
                .map(x -> x.getText())
                .map(s -> s.replaceAll( "([0-9]{2}:[0-9]{2})\n", "$1 "))
                .flatMap(s -> Stream.of(s.split("\n")))
                .map(s -> s.replaceAll("ІНФОГРАФІКА|ДОКУМЕНТ|ОНОВЛЕНО|ФОТО|ВІДЕО|МАПА", ""))
                .filter(s -> !s.contains("PROMOTED"))

                .collect(Collectors.toList());
        return news;
    }
}
