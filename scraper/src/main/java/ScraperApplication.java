import model.News;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ScraperApplication {
    public static void main(String[] args) throws InterruptedException, IOException, ParseException {

        System.setProperty("webdriver.chrome.driver", "scraper/chromedriver.exe");
        Scraper scraper = new Scraper();
        MessageSender messageSender = new MessageSender();

        while (true) {
            System.out.println("Starting producer...");
            List<String> messages = scraper.scrap();

            if (messages.size() > 0) {
                String time = News.fromString(messages.get(0)).getDate();
                Scraper.setLastDate(time);
            }

            messageSender.sendMessages(messages);

            System.out.println("Sleeping for a few minutes...");
            TimeUnit.SECONDS.sleep(120);
        }

    }

}
