import model.News;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test {



    private final static String SOURCE = "http://pravda.com.ua";
    private final static String XPATH = "/html/body/div[4]/div[2]/div[1]/div[3]/div[2]/div[2]/a";
    public static void main(String[] args) throws IOException, ParseException {


        System.setProperty("webdriver.chrome.driver", "scraper/chromedriver.exe");
        System.setProperty("webdriver.chrome.silentOutput", "true");
        ChromeOptions chromeOptions = new ChromeOptions().addArguments("--headless");;
        WebDriver webDriver = new ChromeDriver(chromeOptions);

        webDriver.get(SOURCE);
        List<WebElement> newsWebElements = webDriver.findElements(By.className("article_news"));
        List<String> newsUrls = new ArrayList<>();
        for (WebElement w : newsWebElements) {
            newsUrls.add(w.findElement(By.className("article_header")).findElement(By.tagName("a")).getAttribute("href"));
        }
        System.out.println(newsUrls);
        for (String s : newsUrls) {
            webDriver.navigate().to(s);
            System.out.println(webDriver.findElement(By.className("post_title")).getText());
        }



    }
}
