import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class MessageSender {
    private String queue;

    public MessageSender() { this.queue = "news"; }

    public MessageSender(String queue) { this.queue = queue; }

    private Channel getChannel() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }

    public void sendMessages(List<String> messages) {
        try {
            Channel channel = getChannel();
            channel.queueDeclare(this.queue, false, false, false, null);
            for (String message : messages) {
                channel.basicPublish("", this.queue, null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
            }
            System.out.printf("Successfully sent %d messages to RabbitMQ\n", messages.size());
        } catch (Exception e) {
            System.out.println("Sender faced a problem:");
            e.printStackTrace();
        }
    }
}
