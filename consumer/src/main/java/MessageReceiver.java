import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import model.News;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class MessageReceiver {

    private String queue;
    private MongoService mongoService = new MongoService();
    private List<News> retrievedMessages = new ArrayList<>();

    public MessageReceiver() { this.queue = "news"; }

    public MessageReceiver(String queue) { this.queue = queue; }

    private Channel getChannel() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MongoService.getDefaultHost());
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }

    public void startReceiving() {
        try {
            Channel channel = getChannel();

            channel.queueDeclare(this.queue, false, false, false, null);
            System.out.println(" [*] Waiting for messages");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                this.retrievedMessages.add(News.fromString(message));
                System.out.println(" [x] Received '" + message + "'");
            };
            channel.basicConsume(this.queue, true, deliverCallback, consumerTag -> { });
        }
        catch (Exception e) {
            System.out.println("Receiver faced a problem:");
            e.printStackTrace();
        }
    }

    public void persist()  {
        mongoService.writeMany(this.retrievedMessages);
    }

}
