import java.util.concurrent.TimeUnit;

public class ConsumerApplication {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting consumer...");
        TimeUnit.SECONDS.sleep(15);
        MessageReceiver messageReceiver = new MessageReceiver();
        messageReceiver.startReceiving();

        while (true) {
            messageReceiver.persist();
            TimeUnit.SECONDS.sleep(15);
        }
    }
}
