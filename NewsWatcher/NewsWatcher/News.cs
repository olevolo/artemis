﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace NewsWatcher
{
    public class News
    {
        public string Id { get; set; }
        public string Source { get; set; }
        public string Title { get; set; }
        public string Date { get; set; }

        public bool Liked { get; set; }

        //public bool IsLikedUnset => !Liked.HasValue;
        //public bool IsLikedSet => Liked.HasValue;

        //public string LikedString => Liked.HasValue ? Liked.Value ? "OK" : "Not OK" : "";

        //public bool? Liked
        //{
        //    get
        //    {
        //        return _liked;
        //    }
        //    set
        //    {
        //        _liked = value;
        //    }
        //}

        public News()
        {}

        public News(string id, string source, string title, string date, bool liked)
        {
            Id = id;
            Source = source;
            Title = title;
            Date = date;
            Liked = liked;
        }

        public override string ToString()
        {
            return Date + " " + Source + " " + Title;
        }
    }
}
