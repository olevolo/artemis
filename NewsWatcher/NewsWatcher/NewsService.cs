﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.Client;
using GraphQL.Common.Request;

namespace NewsWatcher
{
    public class NewsService
    {
        protected readonly GraphQLClient _client;
        private readonly string _url = "http://localhost:8080/graphql";

        public NewsService()
        {
            _client = new GraphQLClient(_url);
        }

        public List<News> GetAllNews()
        {
            var request = new GraphQLRequest()
            {
                Query = @"
                    query { 
                        getAllNews { 
                            id, 
                            source, 
                            title,
                            date,
                            liked
                        }
                    }"
            };
            var graphQLResponse = _client.PostAsync(request).Result;
            var news = graphQLResponse.GetDataFieldAs<List<News>>("getAllNews");
            //news.Reverse();
            return news;
        }

        public bool? UpdateWithLike(string id, bool liked)
        {
            var request = new GraphQLRequest()
            {
                Query = @"
                    mutation 
                    updateWithLike($id: String!, $liked: Boolean!) {
                    updateWithLike(id: $id, liked: $liked) {
                      id
                    }
                  }",
                Variables = new { id = id, liked = liked }
            };
            var graphQLResponse = _client.PostAsync(request).Result;
            var news = graphQLResponse.GetDataFieldAs<News>("updateWithLike");
            return news.Liked;
        }
    }
}
