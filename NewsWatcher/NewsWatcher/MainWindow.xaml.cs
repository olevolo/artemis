﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewsWatcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly NewsService _newsService = new NewsService();
        private List<News> _news = new List<News>();

        public MainWindow()
        {
            InitializeComponent();
            _news = _newsService.GetAllNews();
            UpdateListView();
        }

        private void UpdateListView()
        {
            this.newsList.Items.Clear();
            foreach (var news in _news)
            {
                var title = news.Liked ? "Dislike" : "Like";
                var color = news.Liked ? Brushes.Green : Brushes.Red;
                Button likeButton = new Button() { Name = title, Width = 70, Height = 30,
                    Style = FindResource("RoundCorner") as Style ,Content = title, Tag = news.Id, Background = color };
                likeButton.Click += new RoutedEventHandler(Button_Click);
                this.newsList.Items.Add(GetNewsData(news));
                this.newsList.Items.Add(likeButton);
                this.newsList.Items.Add("_________________________________________________________________________________________");
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = (sender as Button);
            var id = button.Tag.ToString();
            var news = _news.FirstOrDefault(x => x.Id == id);

            news.Liked = !news.Liked;
            _news.FirstOrDefault(x => x.Id == id).Liked = news.Liked;

            button.Background = news.Liked ? Brushes.Green : Brushes.Red;
            button.Name = news.Liked ? "Dislike" : "Like";
            button.Content = news.Liked ? "Dislike" : "Like";

            _newsService.UpdateWithLike(id, news.Liked);
        }


        private string GetNewsData(News news)
        {
            return news.ToString();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            _news = _newsService.GetAllNews();
            UpdateListView();
        }
    }
}
