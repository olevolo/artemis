import model.News;
import org.bson.Document;

public class NewsDocument extends Document {

    public NewsDocument(News news) {
        super();
        this.put("source", news.getSource());
        this.put("title", news.getTitle());
        this.put("date", news.getDate());
    }
}
