package model;

import java.io.Serializable;

public class News implements Serializable {

    private String source;
    private String title;
    private String date;

    public News() {
    }

    public News(String source, String date, String title) {
        this.source = source;
        this.date = date;
        this.title = title;
    }

    public static News fromString(String s) {
        return new News(
                "pravda.com.ua",
                s.substring(0,5),
                s.substring(5).trim()
        );
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "News{" +
                "source='" + source + '\'' +
                ", title='" + title + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
