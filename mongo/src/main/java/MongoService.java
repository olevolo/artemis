import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import model.News;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class MongoService {

    private final static String DEFAULT_HOST = "localhost";
    private final static int DEFAULT_PORT = 27017;
    private final static String DEFAULT_DATABASE_NAME = "artemis";
    private final static String DEFAULT_COLLECTION_NAME = "news";

    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    private MongoCollection mongoCollection;

    public MongoService() {
        this.mongoClient = new MongoClient(DEFAULT_HOST, DEFAULT_PORT);
        this.mongoDatabase = mongoClient.getDatabase(DEFAULT_DATABASE_NAME);
        this.mongoCollection = mongoDatabase.getCollection(DEFAULT_COLLECTION_NAME);
    }

    public MongoService(String database, String collection) {
        this.mongoClient = new MongoClient(DEFAULT_HOST, DEFAULT_PORT);
        this.mongoDatabase = mongoClient.getDatabase(database);
        this.mongoCollection = mongoDatabase.getCollection(collection);
    }

    public static String getDefaultHost() { return DEFAULT_HOST; }
    public static int getDefaultPort() { return DEFAULT_PORT; }

    public MongoDatabase getMongoDatabase() { return mongoDatabase; }
    public void setMongoDatabase(MongoDatabase mongoDatabase) { this.mongoDatabase = mongoDatabase; }

    public MongoCollection getMongoCollection() { return mongoCollection; }
    public void setMongoCollection(MongoCollection mongoCollection) { this.mongoCollection = mongoCollection; }

    public List<News> getAll() {
        List<News> newsList = new ArrayList<>();
        MongoCursor<Document> cursor = mongoCollection.find().iterator();
        while (cursor.hasNext()) {
            Document result = cursor.next();
            News news = new News();
            news.setDate(result.get("date").toString());
            news.setSource(result.get("source").toString());
            news.setTitle(result.get("title").toString());
        }
        return newsList;
    }

    public void writeMany(List<News> messages) {
        List<Document> documentList = new ArrayList<>();

        // remove duplicate news
        messages.removeAll(getAll());

        messages.stream().forEach( news -> {
                Document document = new Document();
                document.put("source", news.getSource());
                document.put("title", news.getTitle());
                document.put("date", news.getDate());
                documentList.add(document);
        });

        if (!documentList.isEmpty()) {
            mongoCollection.insertMany(documentList);
            System.out.printf("Successfully persisted %d messages to MongoDB: database = %s, collection = %s\n",
                    documentList.size(), getMongoDatabase().getName(), getMongoCollection().getNamespace());
            messages.clear();
            documentList.clear();
        }
    }
}
