package com.olevolo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "news")
public class News {

    @Id
    private String id;
    private String source;
    private String date;
    private String title;
    private boolean liked;
    private boolean suggested;

    public News() { }

    public News(String source, String date, String title) {
        this.source = source;
        this.date = date;
        this.title = title;
    }

    public News(String id, String source, String date, String title, boolean liked, boolean suggested) {
        this.id = id;
        this.source = source;
        this.date = date;
        this.title = title;
        this.liked = liked;
        this.suggested = suggested;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return date;
    }
    public void setTime(String time) {
        this.date = time;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isLiked() {
        return liked;
    }
    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isSuggested() { return suggested; }
    public void setSuggested(boolean suggested) { this.suggested = suggested; }

    @Override
    public String toString() {
        return "News{" +
                "id='" + id + '\'' +
                ", source='" + source + '\'' +
                ", date='" + date + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}

