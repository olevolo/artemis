package com.olevolo.service;

import com.mongodb.Mongo;
import com.olevolo.model.News;
import com.olevolo.repository.NewsRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NewsService {

    private NewsRepository newsRepository;

    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository ;
    }

    public List<News> getAllNews() {
        System.out.println("getAllNews()");
        return this.newsRepository.findAll();
    }

    public News updateWithLike(String id, boolean liked) {
        System.out.println("updateWithLike");
        Optional<News> opt = this.newsRepository.findById(id);
        News news = opt.get();
        news.setLiked(liked);
        this.newsRepository.save(news);
        return news;
    }
}
