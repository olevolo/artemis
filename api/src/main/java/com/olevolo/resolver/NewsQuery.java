package com.olevolo.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.olevolo.model.News;
import com.olevolo.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NewsQuery implements GraphQLQueryResolver {

    private final NewsService newsService;

    public NewsQuery(NewsService newsService) {
        this.newsService = newsService;
    }

    public List<News> getAllNews() {
        return this.newsService.getAllNews();
    }
}