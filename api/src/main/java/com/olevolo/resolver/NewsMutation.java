package com.olevolo.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.olevolo.model.News;
import com.olevolo.service.NewsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
public class NewsMutation implements GraphQLMutationResolver {

    private final NewsService newsService;

    public NewsMutation(NewsService newsService) { this.newsService = newsService; }

    public News updateWithLike(String id, boolean liked) {
        return this.newsService.updateWithLike(id, liked);
    }
}