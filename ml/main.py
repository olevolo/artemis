from pymongo import MongoClient
from flask import Flask
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

client = MongoClient(host=['localhost:27017'])
db = client["artemis"]


def get_data():
    X = []
    y = []
    data = db['news'].find({"liked": {'$ne': 'null'}}, {"title": 1,  "liked": 1})

    for post in data:
        title = post.get('title', ' ').lower()
        print(title)

        liked = post.get('liked', None)
        if liked:
            liked = 1
        else:
            liked = 0
        X.append(title)
        y.append(liked)
    return X, y

def get_parameters():
    parameters = {
        "vect__max_df": (0.5, 0.75, 1.0),
        "clf__n_estimators": (10, 20, 50, 100),
        "clf__max_depth": (2, 5, 10),
    }
    return parameters


def training_pipeline():
    X, y = get_data()
    pipeline = Pipeline([ ('vect', CountVectorizer()), ('tfidf', TfidfTransformer()),('clf', RandomForestClassifier()),])
    parameters = get_parameters()
    grid_search = GridSearchCV(pipeline, parameters, n_jobs=-1, verbose=1)
    grid_search.fit(X, y)
    print(grid_search.best_score_)
    return grid_search


def prediction_get_data():
    X = []
    ids = []
    data = db['news'].find({}, {"title": 1, "_id": 1})
    for post in data:
        title = post.get('title', ' ').lower()
        id = post.get('_id', None)
        X.append(title)
        ids.append(id)
    return X, ids


def prediction_save(ids, predictions):
    for number, id in enumerate(ids):
        if predictions[number] == 0:
            db['news'].find_one_and_update({"_id": id}, {'$set': {"suggested": False}})
            print("set False")
        else:
            db['news'].find_one_and_update({"_id": id}, {'$set': {"suggested": True}})
            print("set True")


def full_pipeline():
    model = training_pipeline()
    X, ids = prediction_get_data()
    predictions = model.predict(X)
    prediction_save(ids, predictions)
    return model.best_score_


def train():
    full_pipeline()


train()









# text = post.get('text', ' ').lower()
# full_text = title + " " + text