import { Injectable } from '@angular/core';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';

export interface CreateLikeMutationResponse {
  updInfo: any;
  loading: boolean;
}

const CREATE_LIKE_MUTATION = gql`
  mutation($news: NewsInput!, $newsId: ID!) {
    likeNews(news: $news, newId: $newsId) {
      id
      author
      title
      content
      like
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private apollo: Apollo) {}

  getAllNews(): any {}

  updateNews(newsId: string, updatedNews: any): any {
    this.apollo
      .mutate<CreateLikeMutationResponse>({
        mutation: CREATE_LIKE_MUTATION,
        variables: {
          news: updatedNews,
          newsId: newsId,
        },
      })
      .subscribe((response) => {});
  }
}
