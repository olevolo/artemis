import { Component, OnInit, NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { ApiService } from '../api.service';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';

@Component({
  selector: 'app-news-component',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
})
export class NewsComponent implements OnInit {
  newsAll: any[];
  loading = true;
  error: any;
  updatedNews: any;
  id: any;

  constructor(private apollo: Apollo, private api: ApiService) {}

  updateNews(isLiked: boolean, info: any) {
    this.updatedNews = info;
    this.updatedNews.like = isLiked;
    this.api
      .updateNews(this.updatedNews.id, this.updatedNews)
      .subscribe((response) => {});
  }

  ngOnInit() {
    this.apollo
      .query<any>({
        query: gql`
          query {
            getAllNews {
              id
              author
              content
              title
              like
            }
          }
        `,
      })
      .subscribe(
        ({ data, loading }) => {
          this.newsAll = data && data.getAllNews;
          this.loading = loading;
        },
        (error) => {
          this.loading = false;
          this.error = error;
        }
      );
  }
}
